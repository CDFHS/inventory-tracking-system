<?php
	include("nav.php");
?>
<link href="css/mgr.css" rel="stylesheet" type="text/css">
<div class='main'>
<div class="loading">
<div class="circle"></div>
<div class="circle1"></div>
</div>
<script>

$(document).ready(function(){

	$(".loading").hide();
	$("#hide").hide();
	//$('body').css("color","#000");
	$('body').css("background-color","#fff");
	$('.arrow-up').css("border-bottom","#fff");
	//$('.arrow-up').css("border-color","#000");

  
  
  $('table.zui-table2').each(function() {
    var currentPage = 0;
    var numPerPage = 4;
    var $table = $(this);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = $('<div class="pager"></div>');
    for (var page = 0; page < numPages; page++) {
        $('<span class="page-number"></span>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            $(this).addClass('active').siblings().removeClass('active');
        }).appendTo($pager).addClass('clickable');
    }
    $pager.insertBefore($table).find('span.page-number:first').addClass('active');
});


 $('.viewd').bind('click', function() {

        var id=$(this).attr("value");
     //   alert(id);

        var poststr="view="+id;
        $.ajax({
              url:"e_expense.php",
			  type: "post",
              cache:0,
              data:poststr,
              success:function(result){
                     $('.tablecontainer2').html(result);
               }
        }); 
    });
	
   });
	
</script>
	
	
<?php
echo"<h1>Management Tracking Portal</h1>";

echo"
<div class='tablecontainer'>
<h2>Expenses</h2>
<b class='admin'>Pending</b> - Outstanding payment 3+days<br> 
<b class='gray'>Pending</b> - Awaiting payment<br>
<b class='supporter'>Completed</b> - issued Today<br><br>
<table class='zui-table2'>
    <thead>
        <tr>
            <td class='name'>Name</td>
            <td class='posts'>Expense Type</td>
			<td class='acorns'>Amount</td>
            <td class='snaps'>Expense Date</td>
            <td class='ranking'>Status</td>
			<td class='acorns'>Action</td>
		
        </tr>
    <tbody>";
	$allrequests = $conn->prepare("select * from e_expenses,staff,transactions where status = 'pending' and staff.username = e_expenses.username and transaction_id = trans_num ORDER BY `transactions`.`timestamp` ASC");
    $allrequests->execute();
	$count = 0;
	while($row = $allrequests->fetch()) {
		$count++;
		$fn=$row['first_name'];	
		$sn=$row['surname'];	
		$username=$row['username'];	
		$status=$row['status'];	
		$total=$row['total'];
		$timestamp=$row['timestamp'];
		echo" <tr>
            <td class='name'>$fn $sn</td>
            <td class='posts'>Mileage</td>
            <td class='snaps'>£$total</td>
			<td class='acorns'>$timestamp</td>";
		$d1 = date("Y-m-d");
		$d2 = date('Y-m-d',strtotime("$timestamp"));
		$d1 = new DateTime("$d1");
		$d2 = new DateTime("$d2");
		
$difference = $d1->diff($d2);

$days = $difference->format('%a');
if($status == "completed"){
	$colour = 'ranking supporter';
}
elseif($days >=2)
{
	$colour = 'ranking admin';
}
elseif($days <= 1)
{
	$colour = 'ranking member';
}

			echo"
            <td class='$colour'>$status <br>$days day(s)</td>
			<td class='acorns'><button class='viewd' value='$username'>View</button></td>

        </tr>";
	}
	$qry = date("Y-m-d");
	echo$qry;
		$completedrequests = $conn->prepare("select * from e_expenses,staff,transactions where status = 'completed' and STR_TO_DATE(`issued_timestamp`,'%Y-%m-%d') = '$qry' and staff.username = e_expenses.username and transaction_id = trans_num ORDER BY `transactions`.`timestamp` ASC");
    $completedrequests->execute();
	$count = 0;
	while($row = $completedrequests->fetch()) {
		$count++;
		$fn=$row['first_name'];	
		$sn=$row['surname'];	
		$username=$row['username'];	
		$status=$row['status'];	
		$total=$row['total'];
		$timestamp=$row['timestamp'];
		$issuedby=$row['issued_by'];
		echo" <tr>
            <td class='name'>$fn $sn</td>
            <td class='posts'>Mileage</td>
            <td class='snaps'>£$total</td>
			<td class='acorns'>$timestamp</td>";
		$d1 = date("Y-m-d");
		$d2 = date('Y-m-d',strtotime("$timestamp"));
		$d1 = new DateTime("$d1");
		$d2 = new DateTime("$d2");
		
$difference = $d1->diff($d2);

$days = $difference->format('%a');
if($status == "completed"){
	$colour = 'ranking supporter';
}
$staffusr = $conn->prepare("select username from staff as initial where staff_id = '$issuedby' ");
//concat(first_name, ' ', surname)
		$staffusr->execute();
		$staffname = $staffusr->fetchColumn(); 
			echo"
            <td class='$colour'>$status <br><i>($staffname)</i></td>
			<td class='acorns'><button class='viewd' value='$username'>View</button></td>

        </tr>";
	}
	
	
	while($count <4)
	{
		$count++;
		echo"
        <tr>
            <td class='name'></td>
            <td class='posts'></td>
            <td class='snaps'></td>
			<td class='acorns'></td>
            <td class='ranking admin'></td>
			<td class='blank'></td>

        </tr>
		";
		
	}
	echo"
     
	
		
</table>
</div>
<div class='tablecontainer2'></div>
";
?>