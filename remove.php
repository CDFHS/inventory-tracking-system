<?php
session_start();
 
// get the product id
$id = $_GET['id'];
$id = str_pad($id, 10, '0', STR_PAD_LEFT);

 
// remove the item from the array
unset($_SESSION['cart_items'][$id]);
 
// redirect to product list and tell the user it was added to cart
header('Location: item_database.php');
?>