
//<![CDATA[
$(window).load(function(){
	
function Validate() {
    // first we clear any left over error messages
    $('#error p').remove();
    
    // store the error div, to save typing
    var error = $('#error');
    
    // we start by assuming everything is correct
    // if it later turns out not to be, we just set
    // this to false
    var correct = true;


	
	   if ($('#firstfm01').val() == '' && $('#lastfm01').val() == '') {
        error.append('<p>Please provide at least a first name or surname.</p>');
        correct = false;
    }

  if ($('#first_line').val() == '') {
        error.append('<p>No address provided</p>');
        correct = false;
    }

if ($('#delivery').val() == 'empty') {
        error.append('<p>Delivery Not  Selected</p>');
        correct = false;
    }

    // zip codes we know have to be 5 digits, so check the length
    if ($('#postcode').val().length <= 4) {
        error.append('<p>Invalid postcode format</p>');
        correct = false;
    }
  
   if ($('#mphonefm01').val() == '' && $('#hphonefm01').val() == '') {
        error.append('<p>Please provide at least one contact number.</p>');
        correct = false;
    }
  

    // if we haven't found an error, we hide the error message
    if (correct) {
        error.css('display', 'none');

        // clear the result div
        $('#result').empty();

		
		$.ajax({
        url: form.prop('action'),
        method: form.prop('method'),
        data: form.serialize(),
        success: function(){
          
        }
    });
    }
    // otherwise, we show the error
    else {
        error.css('display', 'block');
    }

    return false;
}

$('#contact').submit(Validate);

});//]]> 

/************************************************************************************************************************************/

  
  
  