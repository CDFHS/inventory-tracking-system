<?php
session_start();
/* JPEGCam Test Script */
/* Receives JPEG webcam submission and saves to local file. */
/* Make sure your directory has permission to write files as your web server user! */
if(isset($_SESSION['webcam'])){
	
$filename = $_SESSION['webcam'] . '.png';

}
else{
$filename = '0000000000' . '.jpg';
}
$result = file_put_contents( 'img/items/'.$filename, file_get_contents('php://input') );
if (!$result) {
	print "ERROR: Failed to write data to $filename, check permissions\n";
	exit();
}

$url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/img/items/' . $filename;
print "$url\n";

?>
