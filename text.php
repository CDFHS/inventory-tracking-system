<?php
/*
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

// Your Account SID and Auth Token from twilio.com/console
$sid = 'AC75011f19dc9dbe0f6912a8a5128a2426';
$token = 'b949adf7c66ceec0f1abb65eccb6fe37';
$client = new Client($sid, $token);

// Use the client to do fun stuff like send text messages!
$client->messages->create(
    // the number you'd like to send the message to
    '+447713337538',
    array(
        // A Twilio phone number you purchased at twilio.com/console
        'from' => '+441158243757',
        // the body of the text message you'd like to send
        'body' => "Hey, this is an example to make sure it is working!"
    )
);
echo "Sent message to myself";

*/

?>


<?php
// Get the PHP helper library from twilio.com/docs/php/install
require_once '/twilio-php-master/Twilio/autoload.php'; // Loads the library
use Twilio\Rest\Client;

// Your Account Sid and Auth Token from twilio.com/user/account
$sid = "AC75011f19dc9dbe0f6912a8a5128a2426";
$token = "b949adf7c66ceec0f1abb65eccb6fe37";
$client = new Client($sid, $token);

$number = $client->incomingPhoneNumbers->create(
    array(
        "voiceUrl" => "http://demo.twilio.com/docs/voice.xml",
        "phoneNumber" => "+15005550006"
    )
);

echo $number->sid;
?>
