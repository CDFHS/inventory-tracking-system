---INSTALLATION---

*Unzip folder into the directory of your choosing, referred to as <install> for the rest of this ReadMe
*Open a command prompt (in Windows, go to START, RUN, type in "cmd" and hit OK)
*Type the following in the command prompt:

	java -jar <install> <input file> <output file> <namespace>
	
	<install> is the full directory path of the JAR file, e.g. C:\IRmarkDOS\IRmarkDOS.jar

        <input file> is the XML you wish to calculate the IRmark for

        <output file> is the text file you wish the IRmark result to be written to

        <namespace> is the full namespace contained in the <IRenvelope> element of your XML file

                

So an example of a full command line to run the software would be:

java -jar C:\IRmarkTool\IRmark2007DOS.jar C:\Files\InputFile.xml C:\Files\OuputFile.txt http://www.govtalk.gov.uk/taxation/SA

 
---PREREQUISITES---

 
JAVA 1.5 (or later) runtime environment must be installed - http://java.sun.com/javase/downloads/index_jdk5.jsp


---DISCLAIMER---

This software is developed free of any license restrictions, however it is UNSUPPORTED. Use of this software is entirely 
at the user's own risk.

Any bugs encounted (NOT enhancements) please email sdsteam@hmrc.gsi.gov.uk

 
