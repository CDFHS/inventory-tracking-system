<?php

date_default_timezone_set('Europe/London');
$ctime = date("Y-m-dTH:i:s", time()); 
$cdate = date("Y-m-d", time());   
$now = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
echo $now->format("Y-m-d H:i:s:u") . '<br>';
$pt1 = $now->format("Y-m-d");
$pt2 = $now->format("H:i:s.u");
$gatewaytimestamp = "$pt1";
$gatewaytimestamp .= "T";
$gatewaytimestamp .= "$pt2";


if(isset($_GET['data_request']))
{
 $dom = new DomDocument('1.0', 'UTF-8'); 
$govtalk = $dom->appendChild($dom->createElement('GovTalkMessage'));
$attr = $dom->createAttribute('xmlns');
        $attr->appendChild($dom->createTextNode('http://www.govtalk.gov.uk/CM/envelope'));
        $govtalk->appendChild($attr);
		
		
$envelope = $govtalk->appendChild($dom->createElement('EnvelopeVersion'));

$envelope->appendChild($dom->createTextNode('2.0')); 
				 
$header = $govtalk->appendChild($dom->createElement('Header'));
$messagedetails = $header->appendChild($dom->createElement('MessageDetails'));
$attr = $dom->createElement('Class');
        $attr->appendChild($dom->createTextNode('HMRC-CHAR-CLM'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('Qualifier');
        $attr->appendChild($dom->createTextNode('request'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('Function');
        $attr->appendChild($dom->createTextNode('list'));
        $messagedetails->appendChild($attr);
		$correlationid = $messagedetails->appendChild($dom->createElement('CorrelationID'));
		$attr = $dom->createElement('Transformation');
        $attr->appendChild($dom->createTextNode('XML'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('GatewayTest');
        $attr->appendChild($dom->createTextNode('1'));
        $messagedetails->appendChild($attr);
//$attr = $dom->createElement('GatewayTimestamp');
 //      $attr->appendChild($dom->createTextNode("$gatewaytimestamp"));
 //      $messagedetails->appendChild($attr);

$senderdetails = $header->appendChild($dom->createElement('SenderDetails'));
$idauth = $senderdetails->appendChild($dom->createElement('IDAuthentication'));
$attr = $dom->createElement('SenderID');
        $attr->appendChild($dom->createTextNode('323412300001'));
        $idauth->appendChild($attr);
$auth = $idauth->appendChild($dom->createElement('Authentication'));
$attr = $dom->createElement('Method');
        $attr->appendChild($dom->createTextNode('clear'));
        $auth->appendChild($attr);

$attr = $dom->createElement('Value');
        $attr->appendChild($dom->createTextNode('testing1'));
        $auth->appendChild($attr);


		
$talkdet = $govtalk->appendChild($dom->createElement('GovTalkDetails'));		
$keys = $talkdet->appendChild($dom->createElement('Keys'));

		
$body = $govtalk->appendChild($dom->createElement('Body'));		


	
$dom->formatOutput = true; // set the formatOutput attribute of domDocument to true


 
$ctimexmlformat = date("Y-m-d H.i.s.u", strtotime($ctime)); 



    // save XML as string or file 
   $test1 = $dom->saveXML(); // put string in test1
   $dom->save("IRmarkDOS/generated_request.xml"); // save as file
}

elseif(isset($_GET['submission_poll']))
{
	
 $dom = new DomDocument('1.0', 'UTF-8'); 
$govtalk = $dom->appendChild($dom->createElement('GovTalkMessage'));
$attr = $dom->createAttribute('xmlns');
        $attr->appendChild($dom->createTextNode('http://www.govtalk.gov.uk/CM/envelope'));
        $govtalk->appendChild($attr);
		
		
$envelope = $govtalk->appendChild($dom->createElement('EnvelopeVersion'));

$envelope->appendChild($dom->createTextNode('2.0')); 
				 
$header = $govtalk->appendChild($dom->createElement('Header'));
$messagedetails = $header->appendChild($dom->createElement('MessageDetails'));
$attr = $dom->createElement('Class');
        $attr->appendChild($dom->createTextNode('HMRC-CHAR-CLM'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('Qualifier');
        $attr->appendChild($dom->createTextNode('poll'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('Function');
        $attr->appendChild($dom->createTextNode('submit'));
        $messagedetails->appendChild($attr);
		$correlationid = $messagedetails->appendChild($dom->createElement('CorrelationID'));
		$attr = $dom->createElement('Transformation');
        $attr->appendChild($dom->createTextNode('XML'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('GatewayTest');
        $attr->appendChild($dom->createTextNode('1'));
        $messagedetails->appendChild($attr);
//$attr = $dom->createElement('GatewayTimestamp');
 //      $attr->appendChild($dom->createTextNode("$gatewaytimestamp"));
  //     $messagedetails->appendChild($attr);

$senderdetails = $header->appendChild($dom->createElement('SenderDetails'));
$idauth = $senderdetails->appendChild($dom->createElement('IDAuthentication'));
$attr = $dom->createElement('SenderID');
        $attr->appendChild($dom->createTextNode('323412300001'));
        $idauth->appendChild($attr);
$auth = $idauth->appendChild($dom->createElement('Authentication'));
$attr = $dom->createElement('Method');
        $attr->appendChild($dom->createTextNode('clear'));
        $auth->appendChild($attr);

$attr = $dom->createElement('Value');
        $attr->appendChild($dom->createTextNode('testing1'));
        $auth->appendChild($attr);


		
$talkdet = $govtalk->appendChild($dom->createElement('GovTalkDetails'));		
$keys = $talkdet->appendChild($dom->createElement('Keys'));
		
$body = $govtalk->appendChild($dom->createElement('Body'));		


    $dom->formatOutput = true; // set the formatOutput attribute of domDocument to true

$ctimexmlformat = date("Y-m-d H.i.s.u", strtotime($ctime)); 



    // save XML as string or file 
  // $test1 = $dom->saveXML(); // put string in test1
 //  $dom->save("IRmarkDOS/submission_poll.xml"); // save as file
   


  

   
   
   
   echo"<br>***************************<br><br>";


	  	
	

   

$request = curl_init('https://test-transaction-engine.tax.service.gov.uk/submission');

// send a file
curl_setopt($request, CURLOPT_POST, true);
$fileContent = file_get_contents('IRmarkDOS/submission_poll_example.xml');
curl_setopt($request, CURLOPT_POSTFIELDS, $fileContent);
// output the response
curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
echo curl_exec($request);

// close the session
curl_close($request);


   echo"<br>***************************<br><br>";

}

elseif(isset($_GET['delete_request']))
{
	 $dom = new DomDocument('1.0', 'UTF-8'); 
$govtalk = $dom->appendChild($dom->createElement('GovTalkMessage'));
$attr = $dom->createAttribute('xmlns');
        $attr->appendChild($dom->createTextNode('http://www.govtalk.gov.uk/CM/envelope'));
        $govtalk->appendChild($attr);
		
		
$envelope = $govtalk->appendChild($dom->createElement('EnvelopeVersion'));

$envelope->appendChild($dom->createTextNode('2.0')); 
				 
$header = $govtalk->appendChild($dom->createElement('Header'));
$messagedetails = $header->appendChild($dom->createElement('MessageDetails'));
$attr = $dom->createElement('Class');
        $attr->appendChild($dom->createTextNode('HMRC-CHAR-CLM'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('Qualifier');
        $attr->appendChild($dom->createTextNode('request'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('Function');
        $attr->appendChild($dom->createTextNode('delete'));
        $messagedetails->appendChild($attr);
		$correlationid = $messagedetails->appendChild($dom->createElement('CorrelationID'));
		$attr = $dom->createElement('Transformation');
        $attr->appendChild($dom->createTextNode('XML'));
        $messagedetails->appendChild($attr);
$attr = $dom->createElement('GatewayTest');
        $attr->appendChild($dom->createTextNode('1'));
        $messagedetails->appendChild($attr);
//$attr = $dom->createElement('GatewayTimestamp');
 //      $attr->appendChild($dom->createTextNode("$gatewaytimestamp"));
  //     $messagedetails->appendChild($attr);

$senderdetails = $header->appendChild($dom->createElement('SenderDetails'));
$idauth = $senderdetails->appendChild($dom->createElement('IDAuthentication'));
$attr = $dom->createElement('SenderID');
        $attr->appendChild($dom->createTextNode('323412300001'));
        $idauth->appendChild($attr);
$auth = $idauth->appendChild($dom->createElement('Authentication'));
$attr = $dom->createElement('Method');
        $attr->appendChild($dom->createTextNode('clear'));
        $auth->appendChild($attr);

$attr = $dom->createElement('Value');
        $attr->appendChild($dom->createTextNode('testing1'));
        $auth->appendChild($attr);


		
$talkdet = $govtalk->appendChild($dom->createElement('GovTalkDetails'));		
$keys = $talkdet->appendChild($dom->createElement('Keys'));
		
$body = $govtalk->appendChild($dom->createElement('Body'));		


    $dom->formatOutput = true; // set the formatOutput attribute of domDocument to true

$ctimexmlformat = date("Y-m-d H.i.s.u", strtotime($ctime)); 



    // save XML as string or file 
   $test1 = $dom->saveXML(); // put string in test1
   $dom->save("IRmarkDOS/delete_request.xml"); // save as file
	
}
   ?>