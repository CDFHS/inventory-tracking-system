<?php
//============================================================+
// File name   : example_003.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 003 for TCPDF class
//               Custom Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
				// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
		
		
$img_file = K_PATH_IMAGES.'background.png';
		$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

		// Logo
		$image_file = K_PATH_IMAGES.'header.jpg';
		$this->Image($image_file, 0, 0, 210, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

		
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();

		
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 003');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(20, 40, 11);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);


// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);
//style="border:0.1px solid black;"
// add a page
$pdf->AddPage();
$name= "matthew";
// set some text to print

///*
include('..\..\..\class.php');
// HERE

if(isset($_GET['invoiceid']))
{
 $stmt = $conn2->prepare("SELECT *  from invoices,clients where client_number = client_id and invoice_id =:id");
	 $stmt->bindParam(":id",$_GET['invoiceid']);
    $stmt->execute();	
    while($row = $stmt->fetch()) {
		$iid = $row["invoice_id"];
		$cid = $row["client_number"];
		$sid = $row["staff_id"];
		$invoicenumber = $row["invoice_number"];
		$invoicedate = $row["invoice_date_modified"];
		$raisedate = $row["raise_invoice_date"];
		$displaydate = date("F j, Y",strtotime($raisedate));
		//$invoicedate = date("Y-m-d",strtotime($invoicedate));
		
		$duedate = $row["invoice_date_due_date"];
		$displaydue = date("F j, Y",strtotime($duedate));
		$status = $row["invoice_status"];
		$paytype = $row["payment_method"];
		$clientid = $row["client_id"];
		$fn = $row["first_name"];
		$sn = $row["surname"];
		$fl = $row["first_line"];
		$sl = $row["second_line"];
		$tl = $row["third_line"];
		$pc = $row["post_code"];
		$po = $row["purchase_order"];
		if($row["mobile_number"] != "")
		{
		$selectednumber = $row["mobile_number"];
		}
		else{
			$selectednumber = $row["home_number"];
		}
		$company = $row["company_name"];
		$email = $row["email"];
	$reciept = $row["attach_reciept"];
	
	}
	
}


if($reciept != null)
{
 $stmt = $conn->prepare("SELECT first_name,surname,first_line,second_line,third_line,post_code,mobile_number,home_number  from transactions,customers where customer_details = customer_id and transaction_id =:reciept");
	 $stmt->bindParam(":reciept",$reciept);
    $stmt->execute();	
    while($row = $stmt->fetch()) {
		
		//$invoicedate = $row["invoice_date_modified"];
		//$displaydate = date("F j, Y",strtotime($invoicedate));
		
		$cfn = $row["first_name"];
		$csn = $row["surname"];
		$cfl = $row["first_line"];
		$csl = $row["second_line"];
		$ctl = $row["third_line"];
		$cpc = $row["post_code"];
		if($row["mobile_number"] != "")
		{
		$cselectednumber = $row["mobile_number"];
		}
		else{
			$cselectednumber = $row["home_number"];
		}
	
	}
	
	
 $getinfo = $conn->prepare("SELECT delivery_date from deliveries where transaction_num =:reciept");
	 $getinfo->bindParam(":reciept",$reciept);
    $getinfo->execute();
	$getdel = $getinfo->fetchcolumn();
	if($getdel == null){
	$getdel = "";
	$method = '';
	}
	else{
	$getdel = date("F j, Y",strtotime($getdel));
	$method = 'Delivery';
	}
}
else{
	$cfn = "";
		$csn = "";
		$cfl = "";
		$csl = "";
		$ctl = "";
		$cpc = "";
		$cselectednumber = "";
		$getdel = "";
		$method = "Not Set currently collection";
}

// TO HERE
 $getinfo = $conn->prepare("SELECT concat(first_name,' ', surname) as concatname from staff where staff_id =:staff");
	 $getinfo->bindParam(":staff",$sid);
    $getinfo->execute();
	$getstaff = $getinfo->fetchcolumn();


 $getinfo = $conn2->prepare("select terms_conditions from invoice_terms where attached_invoice = :invid");
	 $getinfo->bindParam(":invid",$iid);
    $getinfo->execute();
	$getterms = $getinfo->fetchcolumn();



$tbl = <<<EOD

<h3 style="color:#888">|| $displaydate</h3>
<br><br>
<h2>Invoice: $invoicenumber</h2>



<table class='comparison-table' style="padding: 5px; border:2px solid grey;" >
<tr style="background-color:#90EE90;color:#000;"><th colspan="2" style="border:1px solid black;">Bill To</th><th colspan="2" style="border:1px solid black;">Ship To</th></tr>
	   <tr style="background-color:;color:#000;" >
		<th>Customer</th>
		<td style="border-right:1px solid black;">
EOD;


if($company != $fl){
	$tbl .= <<<EOD
	$company<br>
EOD;
}

$tbl .= <<<EOD
		
			$fl<br>
			$sl<br>
			$tl<br>
			$pc<br>
			</td>
		<th>Recipient</th>
		<td>$cfn $csn<br>
			$cfl<br>
			$csl<br>
			$ctl<br>
			$cpc<br>
			</td>
	</tr>
	   <tr style="background-color:;color:#000;" >
		<th>Customer PO#</th>
		<td style="border-right:1px solid black;">$po</td>
		<td></td>
		<td></td>
	</tr>
	   <tr style="background-color:;color:#000;" >
		<th>Phone</th>
		<td style="border-right:1px solid black;">$selectednumber</td>
		<th>Phone</th>
		<td>$cselectednumber</td>
	</tr>
	   <tr style="background-color:;color:#000;" >
		<th></th>
		<td style="border-right:1px solid black;"></td>
		<th></th>
		<td></td>
	</tr>
	
<tr style="background-color:#90EE90;color:#000;"><th colspan="2" style="border:1px solid black;">Payment</th><th colspan="2" style="border:1px solid black;">Shipping</th></tr>

	
		   <tr style="background-color:;color:#000; " >
		<th>Payment Due</th>
		<td>$displaydue</td>
		<th>Delivery Date</th>
		<td>$getdel</td>
	</tr>
	
	 <tr style="background-color:;color:#000; " >
		<th>Salesperson</th>
		<td>$getstaff</td>
		<th>Shipping Method</th>
		<td>$method</td>
	</tr>
	
	<tr style="background-color:;color:#000; " >
		<th>Payment Terms</th>
		<td>On Receipt</td>
		<th>Shipping Terms</th>
		<td></td>
	</tr>
	
	
<tr style="background-color:;color:#000;">
<td colspan="2" style="border-top:2px solid black; border-right:1px solid black;">
<br>
Beneficiary:	County Durham Furniture Help Scheme<br><br>
Account no:	65233647<br><br>
Sort Code:	08-92-99<br>

</td>
<td colspan="2" style="border-top:2px solid black;">
 Invoice Terms: <br><br>$getterms<br><br>
 </td>

</tr>

	
	
</table>


<br><br><br>

<table class="table table-condensed" style="background-color:;color:#000; border:2px solid grey; padding: 5px;" nobr="true">
    						<thead>
                                <tr>
								<td width="35"><strong>Qty</strong></td>
        							<td><strong>Item</strong></td>
									<td width="220"><strong>Description</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
EOD;
if($reciept != null){
	
	
$stmt = $conn->prepare("select transaction_id, timestamp, sub_total, delivery_charge, discount_code, total, customers.first_name as cf, customers.surname as cs, staff.first_name as sf, staff.surname as ss, items_purchased  FROM transactions, 
	 staff, customers WHERE customer_id = customer_details and staff_id = sales_rep and transaction_id = :reciept");
	 $stmt->bindParam(":reciept",$reciept);
    $stmt->execute();

	    while($row = $stmt->fetch()) {
		 $ip = $row["items_purchased"];
		 $recieptdis = $row["discount_code"];
		  $recieptsub = $row["sub_total"];
		 $recieptdel = $row["delivery_charge"];
		 $reciepttot = $row["total"];
	}
	
		$tester123 = rtrim($ip,',');
$tl=explode(",",$tester123); 


	 $rowdecode = json_decode($ip);

	 
	 foreach($rowdecode as $row => $quan)
    {
		
	
	// GET DESC GET PRICE CALC TOTS
	//ADD TERMS INTO PAYMENT INFO AND SPLIT


	 $getprice = $conn->prepare("select description, item_price FROM items, reuse, price,
	 staff WHERE reuse_id = reuse_number and staff_id = staff_number and priced_item = item_id and item_id = :id and price_num = price_id");
	 $getprice->bindParam(":id",$row);
    $getprice->execute();
 
    while($subrow = $getprice->fetch()) {
	
		$price = $subrow["item_price"];
		$desc = $subrow["description"];
	$qtot = $quan * $price;
$qtot = number_format($qtot,2);
	}

$tbl .= <<<EOD
 							<tr>
								<td width="30">$quan</td>
    								<td>$row</td>
									<td width="225">$desc</td>
    								<td class="text-center">&pound;$price</td>
    								
    								<td class="text-right">&pound;$qtot</td>
    							</tr>
EOD;
	}
	
	
	
	
	$tbl .= <<<EOD

								


    							<tr>
								
    								<td class="thick-line" colspan="2"><strong></strong></td>
    							<td></td>
    								<td class="thick-line text-center"><strong>Sub Total</strong></td>
									
    								<td class="thick-line text-right">&pound;$recieptsub</td>
    							</tr>
    							<tr>
								<td class="thick-line" colspan="2"><strong>Discount</strong></td>
    								<td>&pound;$recieptdis</td>
    								<td class="no-line text-center"><strong>Delivery</strong></td>
    								<td class="no-line text-right">&pound;$recieptdel</td>
    							</tr>
    							<tr>
								<td></td>
    								<td class="no-line"></td>
    								<td></td>
    								<td class="no-line text-center"><strong>Goods Total</strong></td>
    								<td class="no-line text-right">&pound;$reciepttot</td>
    							</tr>
EOD;
	
	
	}
	else{
		$reciepttot = 0;
	}

// HERE ADD INFO FROM INVOICE EXTRAS

$tbl .= <<<EOD
								<tr>
								<td colspan="2"><strong>Additional</strong></td>
    								<td class="thick-line"></td>
    							
    								<td class="thick-line text-center"></td>
									
    								<td class="thick-line text-right"></td>
    							</tr>
						
						
EOD;
	
		 $customcost = 0;
$additional = $conn2->prepare("select description, cost from invoice_customentry where invoice_key = :key");
	 $additional->bindParam(":key",$iid);
    $additional->execute();

	    while($row = $additional->fetch()) {
		 $customdesc = $row["description"];
		 $customcostrow = $row["cost"];
		$customcost += $customcostrow;
		
		$tbl .= <<<EOD
		<tr>
								
    								
								<td width="30"></td>
    								<td></td>
									<td width="225">$customdesc</td>
    								<td class="text-center"></td>
    								
    								<td class="text-right">&pound;$customcostrow</td>
    							</tr>
						
						
EOD;
		
		
		}

		$fullcalctotal = $reciepttot + $customcost;
		$fullcalctotal = number_format($fullcalctotal,2);
$tbl .= <<<EOD
								<tr>
								<td colspan="2"><strong></strong></td>
    								<td class="thick-line"></td>
    							
    								<td class="thick-line text-center"><strong>Full Invoice Total</strong></td>
									
    								<td class="thick-line text-right">&pound;$fullcalctotal</td>
    							</tr>
								
    						</tbody>
    					</table>
						
EOD;

//*/

//$tbl = <<<EOD
//here
//EOD;


// print a block of text using Write()
//$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
$pdf->writeHTML($tbl, true, false, true, false, '');
// ---------------------------------------------------------

//Close and output PDF document

$pdf->Output($_SERVER['DOCUMENT_ROOT']."inventory/invoice/pdf/$company/"."$invoicenumber.pdf", 'F');
//$pdf->Output("C:\wamp64\www\trackingsystem\inventory\invoice\pdf\$company\$invoicenumber.pdf", 'I');
echo"Saved!";

//============================================================+
// END OF FILE
//============================================================+
