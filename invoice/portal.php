<?php
	include("../nav.php");
?>
<script src="../js/jquery.js"></script>
<link href="../css/tracking.css" rel="stylesheet" type="text/css">
<link href="../css/mgr.css" rel="stylesheet" type="text/css">
<script src="../js/mgr.js"></script>

<div class='main'>
<div class="loading">
<div class="circle"></div>
<div class="circle1"></div>
</div>
<script>

$(document).ready(function(){

	$(".loading").hide();
	$("#hide").hide();
	//$('body').css("color","#000");

	
	//$('.arrow-up').css("border-color","#000");

  
  
  $('table.zui-table2').each(function() {
    var currentPage = 0;
    var numPerPage = 4;
    var $table = $(this);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = $('<div class="pager"></div>');
    for (var page = 0; page < numPages; page++) {
        $('<span class="page-number"></span>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            $(this).addClass('active').siblings().removeClass('active');
        }).appendTo($pager).addClass('clickable');
    }
    $pager.insertBefore($table).find('span.page-number:first').addClass('active');
});


 $('.viewd').bind('click', function() {

        var id=$(this).attr("value");
     //   alert(id);

        var poststr="view="+id;
        $.ajax({
              url:"../e_expense.php",
			  type: "post",
              cache:0,
              data:poststr,
              success:function(result){
                     $('.tablecontainer2').html(result);
               }
        }); 
    });
	
   });
	
</script>
	
	
<?php
echo"<h1>Management Tracking Portal</h1>";


echo"
<div class='tablecontainer'>
<h2>quote overview</h2>

<table width='100%'>
    <thead>
        <tr>
            <td width='80%'>Overview</td>
            <td>This month</td>

		
        </tr>
    <tbody>

        <tr>
            <td>Draft</td>
            <td>£0.00</td>
        </tr>
		<tr>
            <td>Sent</td>
            <td>£0.00</td>
        </tr>
		<tr>
            <td>Viewed</td>
            <td>£0.00</td>
        </tr>
		<tr>
            <td>Approved</td>
            <td>£0.00</td>
        </tr>
		<tr>
            <td>Rejected</td>
           <td>£0.00</td>
        </tr>
		<tr>
            <td>Cancelled</td>
            <td>£0.00</td>
        </tr>
		";
		
	
	echo"
     
</table>
</div>


";


//$paid = $conn2->prepare("select sum()");




echo"
<div class='tablecontainer'>
<h2>Invoice overview</h2>

<table width='100%'>
    <thead>
        <tr>
            <td width='80%'>Overview</td>
            <td>This month</td>

		
        </tr>
    <tbody>";
	$monthdraft = 0;
	$monthsent = 0;
	$monthviewed = 0;
	$monthpaid = 0;
	
	
	
	
		 $stmt = $conn2->prepare("SELECT full_total, invoice_status  from invoices, invoice_amounts where invoice_amounts.invoice_id = invoices.invoice_id and MONTH(`invoice_date_due_date`) = '05' and year(`invoice_date_due_date`) = '2018' order by invoice_date_modified desc");
    $stmt->execute();	
    while($row = $stmt->fetch()) {
		$full_total = $row["full_total"];
		$invoice_status = $row["invoice_status"];
		if($invoice_status == 'draft'){
			$monthdraft += $full_total;
		}
		elseif($invoice_status == 'sent'){
			$monthsent += $full_total;
		}
		elseif($invoice_status == 'viewed'){
			$monthviewed += $full_total;
		}
		elseif($invoice_status == 'paid'){
			$monthpaid += $full_total;
		}
	}
	$monthdraft = number_format($monthdraft, 2);
	$monthsent = number_format($monthsent, 2);
	$monthviewed = number_format($monthviewed, 2);
	$monthpaid = number_format($monthpaid, 2);
	echo"

        <tr>
            <td>Draft</td>
            <td>&pound;$monthdraft</td>
        </tr>
		<tr>
            <td>Sent</td>
            <td>&pound;$monthsent</td>
        </tr>
		<tr>
            <td>Viewed</td>
            <td>&pound;$monthviewed</td>
        </tr>
		<tr>
            <td>Paid</td>
            <td>&pound;$monthpaid</td>
        </tr>
		";
		
	
	echo"
     
</table>
<br><br>
<table width='100%'>
<tr><td width='80%'>Overdue Invoices</td><td>£0.00</td></tr>
</table>

</div>


";


 

echo"
<div class='tablecontainer'>
<h2>Recent Drafts</h2>

<table width='100%'>
    <thead>
        <tr>
            <td>Status</td>
            <td>Date</td>
			 <td>Quote</td>
			 <td>Client</td>
			  <td>Total</td>
        </tr>
    <tbody>
";

	 $stmt = $conn2->prepare("SELECT *  from invoices,clients, invoice_amounts where client_number = client_id and invoice_amounts.invoice_id = invoices.invoice_id and invoice_status = 'draft' order by invoice_date_modified desc");
    $stmt->execute();	
    while($row = $stmt->fetch()) {
		$iid = $row["invoice_id"];
		$fn = $row["first_name"];
		$sn = $row["surname"];
		$invoicedate = $row["invoice_date_modified"];
		$company = $row["company_name"];
		$status = $row["invoice_status"];
		$invoicenumber = $row["invoice_number"];
		$full_total = $row["full_total"];
echo"
        <tr>
            <td>$status</td>
            <td>$invoicedate</td>
			 <td>$invoicenumber</td>
			 <td width='40%'>$fn $sn</td>";
			 if($full_total != null){
				echo"<td>&pound;$full_total
				<form action='viewinvoice.php' method='get'>
				<input type='hidden' value='$invoicenumber' name='invoicenum'>
				<button id='nomargin'>View</button>
				</form>
				</td>";
			 }
			 else{
				echo"<td>&pound;0.00
					<form action='viewinvoice.php' method='get'>
				<input type='hidden' value='$invoicenumber' name='invoicenum'>
				<button id='nomargin' type='submit'>View</button>
				</form>
				</td>"; 
			 }
			  echo"
        </tr>
	
		";
	}	
	
	echo"
     
</table>


</div>


";





echo"
<div class='tablecontainer'>
<h2>Recent Invoices</h2>

<table width='100%'>
    <thead>
        <tr>
            <td>Status</td>
            <td>Due Date</td>
			 <td>Invoice</td>
			 <td>Client</td>
			  <td>Full Total</td>
			  <td>Balance</td>
        </tr>
    <tbody>";


		 $stmt = $conn2->prepare("SELECT *  from invoices,clients, invoice_amounts where client_number = client_id and invoice_amounts.invoice_id = invoices.invoice_id and (invoice_status = 'sent' or invoice_status='viewed') order by invoice_date_modified desc");
    $stmt->execute();	
    while($row = $stmt->fetch()) {
		$iid = $row["invoice_id"];
		$fn = $row["first_name"];
		$sn = $row["surname"];
		$invoicedate = $row["invoice_date_modified"];
		$company = $row["company_name"];
		$status = $row["invoice_status"];
		$invoicenumber = $row["invoice_number"];
		$full_total = $row["full_total"];
		$balance = $row["invoice_balance"];
		
echo"
        <tr>
            <td>$status</td>
            <td>$invoicedate</td>
			 <td>$invoicenumber</td>
			 <td width='40%'>$fn $sn</td>";
			 if($full_total != null){
				echo"<td>&pound;$full_total
				<form action='viewinvoice.php' method='get'>
				<input type='hidden' value='$invoicenumber' name='invoicenum'>
				<button id='nomargin'>View</button>
				</form>
				</td>";
				if($balance != null){
				echo"<td>$balance</td>";
				}
				else{
				echo"<td>$full_total</td>";
				}
			 }
			 else{
				echo"<td>&pound;0.00
					<form action='viewinvoice.php' method='get'>
				<input type='hidden' value='$invoicenumber' name='invoicenum'>
				<button id='nomargin' type='submit'>View</button>
				</form>
				</td>"; 
			 }
			  echo"
        </tr>
	
		";
	}	
	
	echo"
     
</table>


</div>


";









?>