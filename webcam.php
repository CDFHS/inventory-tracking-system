	<link href="css/tracking.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/webcam1.js"></script>

    <script>

        webcam.set_api_url('upload.php');
        webcam.set_quality( 90 ); // JPEG quality (1 - 100)
        webcam.set_shutter_sound( true ); // play shutter click sound
        
        webcam.set_hook( 'onComplete', 'my_completion_handler' );
		
        
        function take_snapshot() {
	

            // take snapshot and upload to server
            document.getElementById('upload_results').innerHTML = 'Snapshot<br>'+
            '<img src="uploading.gif">';
            webcam.snap();
        }
        
        function my_completion_handler(msg) {
            // extract URL out of PHP output
            if (msg.match(/(http\:\/\/\S+)/)) {
                var image_url = RegExp.$1;
                // show JPEG image in page
                document.getElementById('upload_results').innerHTML = 
                    'Snapshot<br>' + 
                    '<a href="'+image_url+'" target"_blank"><img src="' + image_url + '"></a>';
                
                // reset camera for another shot
                webcam.reset();
				
            }
            else alert("PHP Error: " + msg);
        }

    </script>

	<?php
session_start();
include('class.php');

if(isset($_GET['itemid'])){
	
	$itemid = $_GET['itemid'];
$itemid = str_pad($itemid, 10, '0', STR_PAD_LEFT);
$_SESSION['webcam'] = $itemid;

	
}

?>

	
	<table class="main">
        <tr>
            <td valign="top">
	            <div class="border">
                Live Webcam<br>
                <script>
                document.write( webcam.get_html(320, 240) );
                </script>
                </div>
                <br/><input type="button" class="snap" value="Take Picture" onClick="take_snapshot(); setTimeout(function(){   window.top.location.reload();}, 1000);    ">
            </td>
            <td width="50">&nbsp;</td>
            <td valign="top">
                <div id="upload_results" class="border">
                    Snapshot<br>
                    <img src="logo.jpg" />
                </div>
            </td>
        </tr>
    </table>

