<?php
// Including all required classes
require_once('class/BCGFontFile.php');
require_once('class/BCGColor.php');
require_once('class/BCGDrawing.php');

// Including the barcode technology
require_once('class/BCGcode39.barcode.php');
if(isset($_GET['wd1']))
{
	$wd1 = $_GET['wd1'];
}
if(isset($_GET['reftot']))
{
	$reftot = $_GET['reftot'];
}
if(isset($_GET['vegtot']))
{
	$vegtot = $_GET['vegtot'];
}
else{
	
		$vegtot = null;
}
// Loading Font
$font = new BCGFontFile('./font/Arial.ttf', 0);
$tid = $_GET['tid'];
if($wd1 == 'returns'){
	$tid = str_pad($tid, 11, '0', STR_PAD_LEFT);
	}
else{
$tid = str_pad($tid, 12, '0', STR_PAD_LEFT);
}
// Don't forget to sanitize user inputs
$text = isset($_GET['text']) ? $_GET['text'] : "$tid";

// The arguments are R, G, B for color.
$color_black = new BCGColor(0, 0, 0);
$color_white = new BCGColor(255, 255, 255);

$drawException = null;
try {
    $code = new BCGcode39();
    $code->setScale(2); // Resolution
    $code->setThickness(40); // Thickness
    $code->setForegroundColor($color_black); // Color of bars
    $code->setBackgroundColor($color_white); // Color of spaces
    $code->setFont($font); // Font (or 0)
    $code->parse($text); // Text
	
} catch(Exception $exception) {
    $drawException = $exception;
}

/* Here is the list of the arguments
1 - Filename (empty : display on screen)
2 - Background color */
$drawing = new BCGDrawing("thermal.png", $color_white);
if($drawException) {
    $drawing->drawException($drawException);
} else {
    $drawing->setBarcode($code);
    $drawing->draw();
	
}


$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);



if($wd1 == 'returns'){
header("location:testret.php?tid=$tid&wd1=$wd1&reftot=$reftot");
}
elseif($wd1 == 'vegcash'){
	$_SESSION['norefresh'] = "AbCkD45";
header("location:testveg.php?tid=$tid&vegtot=$vegtot");

}

else{
header("location:test.php?tid=$tid&wd1=$wd1&reftot=$reftot");
}

?>