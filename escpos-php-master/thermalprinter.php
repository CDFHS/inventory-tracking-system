 <?php
 
require_once(dirname(__FILE__) . "/Escpos.php");
try {
    // Enter the share name for your USB printer here
    $connector = new WindowsPrintConnector("smb://UNIT20-PC7/POS");
    /* Print a "Hello world" receipt" */
    $printer = new Escpos($connector);
$img = new EscposImage("logo.png");
		   $printer -> bitImage($img);
$printer -> feed();

$printer -> text("County Durham Furniture Help \nScheme\n\n");
$printer -> text("Unit 19\n");
$printer -> text("Avenue 3,\n");
$printer -> text("Chilton, \n");
$printer -> feed();
	$printer -> pulse();
    /* Close printer */
    $printer -> close();
} catch(Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}